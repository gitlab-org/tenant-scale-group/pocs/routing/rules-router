import { parse } from 'cookie';

export default {
	async fetch(request, env) {
		const cell_1 = new URL(env.CELL_1);
		const cell_2 = new URL(env.CELL_2);

		// We'll get this from KV or somewhere else.
		const routingRules = {
			rules: [
				{
					id: '0635599ec388',
					headers: {
						'X-Gitlab-Cell-Redirect': {
							prefix: 'gdk-1',
						},
					},
					action: 'proxy',
					priority: 500,
				},
				{
					id: '0c3f073fe5db',
					cookie: {
						key: '_gitlab_session',
						prefix: 'cell_1_',
						target: cell_1.toString(),
					},
					action: 'proxy',
					priority: 100,
				},
				{
					id: '13c059085c3d',
					header: {
						key: 'private-token',
						contains: 'cell_1_',
						target: cell_1.toString(),
					},
					action: 'proxy',
					priority: 100,
				},
				{
					id: '3e4aa147a825',
					header: {
						key: 'job-token',
						contains: 'cell_1_',
						target: cell_1.toString(),
					},
					action: 'proxy',
					priority: 100,
				},
				{
					id: '27913e58ff2',
					header: {
						key: 'authorization',
						contains: 'cell_1',
						target: cell_1.toString(),
					},
					action: 'proxy',
					priority: 100,
				},
				{
					id: '0c3f073fe5db',
					cookie: {
						key: '_gitlab_session',
						prefix: 'cell_2_',
						target: cell_2.toString(),
					},
					action: 'proxy',
					priority: 300,
				},
				{
					id: '13c059085c3d',
					header: {
						key: 'private-token',
						contains: 'cell_2_',
						target: cell_2.toString(),
					},
					action: 'proxy',
					priority: 300,
				},
				{
					id: 'df2eb42538c2',
					header: {
						key: 'job-token',
						contains: 'cell_2_',
						target: cell_2.toString(),
					},
					action: 'proxy',
					priority: 100,
				},
				{
					id: '32adf5f0a84c',
					header: {
						key: 'authorization',
						contains: 'cell_2',
						target: cell_2.toString(),
					},
					action: 'proxy',
					priority: 100,
				},
				{
					id: '03b8327a8f51',
					cookie: {
						key: 'cell_2',
						target: cell_2.toString(),
					},
					action: 'proxy',
					priority: 200,
				},
				{
					id: '06351119e388',
					path: {
						prefix: '/',
						match_regex: '^/(?<project_path>.+)\\.git',
					},
					method: ['GET'],
					action: 'classify',
					priority: 50,
					classify: {
						keys: ['project_path'],
					},
				},
			],
		};

		let resolveOverride = cell_1.host;
		let choosenRule = {};

		// Look at rules and decide which rule to use highest priority wins.
		routingRules.rules.sort((a, b) => b.priority - a.priority);
		for (let i = 0; i < routingRules.rules.length; i++) {
			const rule = routingRules.rules[i];
			let target = '';

			if (rule.cookie) {
				target = cookieRule(rule, parse(request.headers.get('Cookie') || ''));
			}

			if (rule.header) {
				target = headerRule(rule, request.headers);
			}

			if (rule.path) {
				const url = new URL(request.url);
				target = await pathRule(rule, env, url.pathname);
			}

			if (target != '') {
				resolveOverride = new URL(target).host;
				choosenRule = rule;
				break;
			}
		}

		console.log({ resolveOverride: resolveOverride, choosenRule: choosenRule });

		// Change just the host
		const url = new URL(request.url);
		const originalHost = url.host;
		url.host = resolveOverride;

		// Best practice is to always use the original request to construct the new request
		// to clone all the attributes. Applying the URL also requires a constructor
		// since once a Request has been constructed, its URL is immutable.
		const newRequest = new Request(url.toString(), request);

		// Set this to enable application to generate correct links
		newRequest.headers.set('X-Forwarded-Host', originalHost);

		return fetch(newRequest);
	},
};

function cookieRule(rule, cookies) {
	let target = '';

	if (!cookies[rule.cookie.key]) {
		return target;
	}

	if (!rule.cookie.hasOwnProperty('prefix')) {
		target = rule.cookie.target;
	}

	if (cookies[rule.cookie.key].startsWith(rule.cookie.prefix)) {
		target = rule.cookie.target;
	}

	return target;
}

function headerRule(rule, headers) {
	let target = '';

	if (!headers.has(rule.header.key)) {
		return target;
	}

	if (headers.get(rule.header.key).includes(rule.header.contains)) {
		target = rule.header.target;
	}

	if (rule.header.key == 'authorization') {
		const authType = headers.get(rule.header.key).split(' ')[0];
		if (authType == 'basic') {
			const base64Credentials = headers.get(rule.header.key).split(' ')[1];
			const credentials = atob(base64Credentials);

			const user = credentials.split(':')[0];
			if (user.includes(rule.header.contains)) {
				target = rule.header.target;
			}

			return target;
		}

		const token = headers.get(rule.header.key).split(' ')[1];
		if (token.includes(rule.header.contains)) {
			target = rule.header.target;
		}
	}

	return target;
}

async function pathRule(rule, env, path) {
	let target = '';
	var re = new RegExp(rule.path.match_regex);
	var matchResult = re.exec(path);
	if (matchResult) {
		var groups = matchResult.groups;
		var key = rule.classify.keys[0];
		if (key == 'project_path') {
			return await classifyProjectPath(env, groups[key]);
		}
	}
	return target;
}

async function classifyProjectPath(env, path) {
	const cell_1 = new URL(env.CELL_1);
	const cell_2 = new URL(env.CELL_2);

	const cells = [cell_1, cell_2];

	for (var i = 0; i < cells.length; i++) {
		var cell_url = cells[i];

		const endpoint = '/api/v4/internal/cells/classify';

		const payload = {
			metadata: { rule_id: 'todo', path: path },
			keys: { project_path: path },
		};

		// Build the full URL
		const url = new URL(endpoint, cell_url.origin);

		// Set HTTP options
		const options = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(payload),
		};

		try {
			console.log(url);
			const response = await fetch(url, options);
			console.log(response);

			// Check for successful response
			if (!response.ok) {
				throw new Error(`Error calling external service: ${response.statusText}`);
			}

			// Parse the JSON response
			const responseData = await response.json();

			// Use the parsed data
			console.log('Received data:', responseData);

			if (responseData.message.action == 'proxy') {
				return cell_url.toString();
			}
		} catch (error) {
			console.log('Caught an error');
			console.error('Error:', error);

			// Handle errors appropriately, e.g., return an error response
			return new Response(JSON.stringify({ message: 'Error calling external service' }), { status: 500 });
		}
	}

	// if (path.startsWith("omar-test-1/")) { return cell_1.toString(); }
	// else if (path.startsWith("omar-test-2/")) { return cell_2.toString(); }
}
