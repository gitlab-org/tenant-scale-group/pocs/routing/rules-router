## :warning: PoC now deprecated

:warning: This project is now deprecated.

Eventually, https://gitlab.com/gitlab-org/cells/router will contain all required functionality.

# Rules Routing

- [Proposal](https://docs.gitlab.com/ee/architecture/blueprints/cells/routing-service.html#proposal)
- [PoC Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/433471)

[[_TOC_]]

## Force cell 2

1. Inside of developer tools set the following cookie:

    ```js
    document.cookie = 'cell_2=true;path=/;domain=gitlab.steveazz.xyz';
    ```

1. To change back delete all cookies for the domain.

## Local Development

We need 2 seperate GDK installed.

### Requirement

Make sure you have an entry for `gdk.test` in your `/etc/hosts` file

```
127.0.0.1             gdk.test
```

### Step 1: gdk 1

1. Install [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md) if you don't have one installed
1. Set up [local interface](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/local_network.md#local-interface)

    ```sh
    gdk config set hostname gdk.test

    gdk config set gitlab.rails.session_store.cookie_key _gitlab_session
    gdk config set gitlab.rails.session_store.unique_cookie_key_postfix false
    gdk config set gitlab.rails.session_store.session_cookie_token_prefix cell_1_

    gdk reconfigure

    gdk restart
    ```

### Step 2: gdk 2

1. Create another installation of GDK

    ```sh
    git clone git@gitlab.com:gitlab-org/gitlab-development-kit.git gitlab-development-kit-01
    cd gitlab-development-kit-01/

    cp ../gitlab-development-kit-00/gdk.yml . # copy existing config
    cp ../gitlab-development-kit-00/lefthook-local.yml . # copy any local lefthook (like disabling asdf)

    gdk config set port 3001
    gdk config set gitlab.rails.session_store.cookie_key _gitlab_session
    gdk config set gitlab.rails.session_store.unique_cookie_key_postfix false
    gdk config set gitlab.rails.session_store.session_cookie_token_prefix cell_2_
    # If you have postgresql.host set, you need to set postgresql.port to a different value than the default
    #
    # gdk config set postgresql.port 5433

    gdk install gitlab_repo=git@gitlab.com:gitlab-org/gitlab.git

    gdk restart
    ```

### Step 3: Run router

1. Install [Prerequisites](https://developers.cloudflare.com/workers/get-started/guide/#prerequisites)
1. Clone and start router

    ```sh
    git clone git@gitlab.com:gitlab-org/tenant-scale-group/pocs/routing/rules-router.git
    cd rules-router
    npm install
    npx wrangler dev --port 9393
    ```

1. Go to `http://gdk.test:9393`

## Deployment

### Step 1: Gitlab-1

1. Create GCP instance

    ```sh
    gcloud --project eng-core-tenant-poc-bbc34148 compute instances create gitlab-1 --image-family ubuntu-2204-lts --image-project ubuntu-os-cloud --zone=us-east1-c --machine-type=n1-standard-4
    ```

1. [Install GitLab](https://about.gitlab.com/install/#ubuntu)

    ```sh
    gcloud --project eng-core-tenant-poc-bbc34148 compute ssh gitlab-1

    steve@gitlab-1:~$ sudo apt-get update
    steve@gitlab-1:~$ sudo apt-get install -y curl openssh-server ca-certificates tzdata perl

    steve@gitlab-1:~$  curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash

    steve@gitlab-1:~$ sudo EXTERNAL_URL="https://gitlab.EXTERNAL_IP_OF_MACHINE.nip.io" apt-get install gitlab-ee

    steve@gitlab-1:~$ sudo cat cat /etc/gitlab/initial_root_password
    ```

### Step 1: Gitlab-2

1. Create GCP instance

    ```sh
    gcloud --project eng-core-tenant-poc-bbc34148 compute instances create gitlab-2 --image-family ubuntu-2204-lts --image-project ubuntu-os-cloud --zone=us-east1-c --machine-type=n1-standard-4
    ```

1. [Install GitLab](https://about.gitlab.com/install/#ubuntu)

    ```sh
    gcloud --project eng-core-tenant-poc-bbc34148 compute ssh gitlab-2

    steve@gitlab-2:~$ sudo apt-get update
    steve@gitlab-2:~$ sudo apt-get install -y curl openssh-server ca-certificates tzdata perl

    steve@gitlab-2:~$ curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash

    steve@gitlab-2:~$ sudo EXTERNAL_URL="https://gitlab.EXTERNAL_IP_OF_MACHINE.nip.io" apt-get install gitlab-ee

    steve@gitlab-2:~$ sudo cat cat /etc/gitlab/initial_root_password
    ```

1. Patch rails to to generate session key with a suffix

    ```sh
    gcloud --project eng-core-tenant-poc-bbc34148 compute ssh gitlab-2 -- sudo sed -i 's/"_gitlab_session"/"_gitlab_session_ddd"/g' /opt/gitlab/embedded/service/gitlab-rails/config/initializers/session_store.rb
    gcloud --project eng-core-tenant-poc-bbc34148 compute ssh gitlab-2 -- sudo gitlab-ctl restart
    ```

### Step 3: Deploy Cloudflare Worker

1. Run `npx wrangler deploy`

## Demo

Recording: <https://www.youtube.com/watch?v=taTBQEBiny4>

<details>
<summary> script </summary>

1. Context
    - Blueprint: <https://docs.gitlab.com/ee/architecture/blueprints/cells/routing-service.html>
    - Cell 1.0: <https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139519>
    - `gitlab-1`: <https://gitlab.35.231.181.234.nip.io>
    - `gitlab-2`: <https://gitlab.35.231.173.179.nip.io>
    - `router`: <https://rules-router.sxuereb.workers.dev>
1. Log into `gitlab-1`
    - <https://rules-router.sxuereb.workers.dev>
    - Show header
    - Log in
    - Click around
    - Log out
    - Delete cookies
1. Log into `gitlab-2`
    - <https://rules-router.sxuereb.workers.dev/poc/gitlab-1>
    - Log in
    - Show header
    - Click around
1. Dive into the cloudflare dashboard: <https://dash.cloudflare.com/1a7f12af9c5a32804050e7cfe7ca313e/workers/services/view/rules-router/production>
1. Walk through the code

</details>

## Benchmarking

The methodology behind this quick benchmark is to take the full duration (ms) of a request,
and subtract the duration (ms) the `fetch` took inside of the worker:

```math
\mathrm{Worker\ latency}\ {ms} = \mathrm{duration}\ {ms} - \mathrm{origin}\ {ms}
```

This is not full proof, because it also accounts for Cloudflare's own infrastructure which we already incur the cost (ms) of for GitLab.com

![image showing latency buckets](https://gitlab.com/gitlab-org/gitlab/uploads/3eec46d8cac5b13e997e0f279d19ac4c/image.png)

1. In 1 terminal window save the worker logs.

    ```sh
    npx wrangler tail --format json > /tmp/wrangler.txt
    ```

1. In another terminal window run `k6`, and save logs in a file.

    ```sh
    k6 run -e URL=https://rules-router.sxuereb.workers.dev/users/sign_in k6.js 2>&1 | rg 'user' > /tmp/k6.txt
    ```

1. Create `csv` from `k6` with `x-unique-id,duration_ms``:

    ```sh
    # parse output and sort it by x-unique-id
    cat /tmp/k6.txt | awk '{print $3}' | awk -F"\"" '{print $2}' | sort  > ~/Downloads/latency.csv
    ```

1. Get the `orign_ms` from worker logs and add them to the csv

    ```sh
    # get log messages and sort by x-unqiue-id
    cat /tmp/wrangler.txt | jq -r '.logs.[0].message.[0]' | sort | awk -F, '{print $2}' | pbcopy
    ```

1. [Example Google Sheet](https://docs.google.com/spreadsheets/d/1j6OX05whq_Go_d2WCYOBqCav7HxW4arArMUDXGkRn9g/edit?usp=sharing)

### Testing from `us-east1`

1. Create a dedicated machine

    ```sh
    gcloud --project eng-core-tenant-poc-bbc34148 compute instances create k6 --image-family ubuntu-2204-lts --image-project ubuntu-os-cloud --zone=us-east1-c --machine-type=n1-standard-4
    ```

1. Install k6

    ```sh
    gcloud --project eng-core-tenant-poc-bbc34148 compute ssh k6
    sudo gpg -k
    sudo gpg --no-default-keyring --keyring /usr/share/keyrings/k6-archive-keyring.gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C5AD17C747E3415A3642D57D77C6C491D6AC1D69
    echo "deb [signed-by=/usr/share/keyrings/k6-archive-keyring.gpg] https://dl.k6.io/deb stable main" | sudo tee /etc/apt/sources.list.d/k6.list
    sudo apt-get update
    sudo apt-get install k6
    ```

1. Get script

    ```sh
    curl -L --output k6.js https://gitlab.com/gitlab-org/tenant-scale-group/pocs/routing/rules-router/-/raw/main/k6.js
    ```

1. Go to [bencharmking](#benchmarking)

### Move everything under Cloudflare

1. Have a domain managed by Cloudflare, example; steveazz.xyz
1. Create an `A` record for `gitlab1.steveazz.xyz` pointing to gitlab1 instance
1. Create an `A` record for `gitlab2.steveazz.xyz` pointing to gitlab2 instance
1. Move Cloudflare worker under a subdomain `gitlab.steveazz.xyz`: <https://developers.cloudflare.com/workers/configuration/routing/custom-domains/#add-a-custom-domain>
1. Update `external_url` for each gitlab instance to be `gitlab1.steveazz.xyz` and `gitlab2.steveazz.xyz` respectivly.

The environment is set up, we need to run load tests on the `k6` machine:

1. SSH inside of the k6 machine

    ```sh
    gcloud --project eng-core-tenant-poc-bbc34148 compute ssh k6
    ```

1. Run 5x for `gitlab1.steveazz.xyz`

    ```sh
    k6 run -e URL=https://gitlab1.steveazz.xyz/users/sign_in k6.js
    ```

1. Run 5x for `gitlab.steveazz.xyz`

    ```sh
    k6 run -e URL=https://gitlab.steveazz.xyz/users/sign_in k6.js
    ```
