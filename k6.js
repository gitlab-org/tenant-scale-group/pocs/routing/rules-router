// https://k6.io/open-source/
import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
	vus: 10,
	duration: '30s',
};

export default function () {
	const uniqueId = `user_${__VU}_${__ITER}`;
	const headers = { 'X-Unique-ID': uniqueId };

	const resp = http.get(__ENV.URL, { headers });

	// CSV with `unique_id,duration_ms`
	console.log(`${uniqueId},${resp.timings.duration}`);

	sleep(1);
}
